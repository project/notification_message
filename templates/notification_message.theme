<?php

/**
 * @file
 * Define theme functions for the notification message module.
 */

use Drupal\Core\Render\Element;

/**
 * Define the template preprocess for the notification messages.
 *
 * @param array $variables
 *   An array of template variables.
 */
function template_preprocess_notification_message(&$variables) {
  $elements = $variables['elements'];
  /** @var \Drupal\notification_message\Entity\NotificationMessage $entity */
  $entity = $elements['#notification_message'];
  $route_name = \Drupal::routeMatch()->getRouteName();

  $variables['message'] = $entity;
  $variables['bundle'] = $entity->bundle();
  $variables['view_mode'] = $elements['#view_mode'];
  $variables['is_message_entity'] = $route_name === 'entity.notification_message.canonical';

  $variables['notification_dismiss'] = [];
  if ($bundle_entity_type = $entity->getBundleEntityTypeEntity()) {
    $variables['notification_dismiss'] = $bundle_entity_type->getNotificationDismiss();
  }

  // Helpful $content variable for templates.
  $variables += ['content' => []];
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

/**
 * Define the template preprocess for the notification messages.
 *
 * @param array $variables
 *   An array of template variables.
 */
function template_preprocess_notification_messages(&$variables) {
  $element = $variables['element'];
  $messages = $element['#messages'] ?? [];

  $variables['count'] = count($messages);
  $variables['messages'] = $messages;
}
