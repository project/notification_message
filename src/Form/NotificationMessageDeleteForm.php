<?php

namespace Drupal\notification_message\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Define the Notification Message Delete Form class.
 */
class NotificationMessageDeleteForm extends ContentEntityDeleteForm {}
