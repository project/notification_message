<?php

namespace Drupal\notification_message\Entity;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Define the notification message access control handler.
 */
class NotificationMessageAccess extends EntityAccessControlHandler {

  /**
   * {@inheritDoc}
   */
  protected function checkAccess(
    EntityInterface $entity,
    $operation,
    AccountInterface $account
  ) {
    if (($admin_permission = $entity->getEntityType()->getAdminPermission())
      && $account->hasPermission($admin_permission)
    ) {
      $result = AccessResult::allowedIfHasPermission(
        $account,
        $admin_permission
      );
    }
    elseif ($operation === 'view') {
      /** @var \Drupal\notification_message\Entity\NotificationMessageInterface $entity */
      $status = $entity->isPublished();
      $uid = $entity->getAuthorUser()->id();
      $result = AccessResult::allowedIf($status)->addCacheableDependency($entity);

      if (!$status) {
        if ($uid === $account->id()) {
          $result = $result->orIf(AccessResult::allowedIfHasPermission(
            $account,
            'view own unpublished notification message')
          );
        }
        $result->orIf(AccessResult::allowedIfHasPermission(
          $account, 'view any unpublished notification message'
        ));
      }
    }

    return $result ?? AccessResult::neutral();
  }

}
