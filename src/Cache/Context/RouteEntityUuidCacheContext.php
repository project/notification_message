<?php

declare(strict_types=1);

namespace Drupal\notification_message\Cache\Context;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\Entity\EntityBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Define the route entity UUID cache context.
 */
class RouteEntityUuidCacheContext implements CacheContextInterface {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Define class constructor.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    RouteMatchInterface $route_match,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->routeMatch = $route_match;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function getLabel(): TranslatableMarkup {
    return new TranslatableMarkup('Route Entity ID');
  }

  /**
   * {@inheritDoc}
   */
  public function getContext(): string {
    $entities = $this->entityCacheTagsFromRoute();
    return !empty($entities) ? implode('|', $entities) : 'none';
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheableMetadata(): CacheableMetadata {
    return new CacheableMetadata();
  }

  /**
   * Build entity cache tags from the current route.
   *
   * @return array
   *   An array of cache tags.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function entityCacheTagsFromRoute(): array {
    /** @var \Drupal\Core\Entity\EntityBase[] $entities */
    $entities = [];
    $cache_tags = [];

    $this->attachParameterEntities($entities);
    $this->attachViewRouteEntities($entities);

    foreach ($entities as $entity) {
      $cache_tags[] = "{$entity->getEntityTypeId()}.{$entity->bundle()}.{$entity->uuid()}";
    }

    return $cache_tags;
  }

  /**
   * Attach parameter entities.
   *
   * @param array $entities
   *    An array of entities.
   *
   * @return void
   */
  protected function attachParameterEntities(array &$entities): void {
    foreach ($this->routeMatch->getParameters() as $entity) {
      if ($entity instanceof EntityBase) {
        $entities[] = $entity;
      }
    }
  }

  /**
   * Attach view route entities.
   *
   * @param array $entities
   *   An array of entities.
   *
   * @return void
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function attachViewRouteEntities(
    array &$entities
  ): void {
    $route_object = $this->routeMatch->getRouteObject();

    if ($view_route_args = $route_object->getOptions()['_view_argument_map'] ?? NULL) {
      foreach ($view_route_args as $entity_type) {
        if ($entity_id = $this->routeMatch->getRawParameter($entity_type)) {
          $entities[] = $this->entityTypeManager->getStorage($entity_type)->load(
            $entity_id
          );
        }
      }
    }
  }

}
