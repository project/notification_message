<?php

namespace Drupal\notification_message\Plugin\Block;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\notification_message\Entity\NotificationMessage;
use Drupal\notification_message\Entity\NotificationMessageType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Define the notification message queue block.
 *
 * @Block(
 *   id = "notification_message",
 *   admin_label = @Translation("Notification messages")
 * )
 */
class NotificationMessageBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The context repository.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected $contextRepository;

  /**
   * NotificationMessageBlock constructor.
   *
   * @param array $configuration
   *   The block configurations.
   * @param string $plugin_id
   *   The block plugin identifier.
   * @param array $plugin_definition
   *   The block plugin definition.
   * @param \Drupal\Core\Plugin\Context\ContextRepositoryInterface $context_repository
   *   The context repository service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    ContextRepositoryInterface $context_repository,
    EntityTypeManagerInterface $entity_type_manager,
    EntityDisplayRepositoryInterface $entity_display_repository
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->contextRepository = $context_repository;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('context.repository'),
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return [
      'notification_message' => [
        'type' => [],
        'display_mode' => 'full',
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * Get notification message type.
   *
   * @return string
   *   The notification message type.
   */
  public function getNotificationMessageType() {
    return $this->configuration['notification_message']['type'];
  }

  /**
   * Get notification message display mode.
   *
   * @return string
   *   The notification message display view mode.
   */
  public function getNotificationMessageDisplayMode() {
    return $this->configuration['notification_message']['display_mode'];
  }

  /**
   * {@inheritDoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $form['notification_message'] = [
      '#type' => 'details',
      '#title' => $this->t('Notification Message'),
      '#open' => TRUE,
    ];
    $form['notification_message']['display_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Display Mode'),
      '#required' => TRUE,
      '#description' => $this->t('Select the notification message view mode.'),
      '#options' => $this->getDisplayModeOptions(),
      '#default_value' => $this->getNotificationMessageDisplayMode(),
    ];
    $form['notification_message']['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Message Type'),
      '#multiple' => TRUE,
      '#description' => $this->t('Select the notification message types.<br/><strong>Note:</strong> If no message types are selected then all valid messages are rendered.'),
      '#options' => $this->getNotificationMessageTypeOptions(),
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $this->getNotificationMessageType(),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $this->configuration['notification_message'] = $form_state->getValue('notification_message');
  }

  /**
   * {@inheritDoc}
   */
  public function build(): array {
    $build = [];

    if ($messages = $this->loadMessages()) {
      $build = [
        '#block' => $this,
        '#theme' => 'notification_messages',
        '#messages' => $this->viewMessages($messages),
      ];

      $build['#attached']['library'][] = 'notification_message/notification.dismiss';
    }

    return $build;
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheTags(): array {
    $tags = parent::getCacheTags();
    $tags[] = 'notification_message_list';

    foreach ($this->loadMessages() as $message) {
      $tags = Cache::mergeTags($tags, $message->getCacheTags());
    }

    return $tags;
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheContexts(): array {
    $contexts = parent::getCacheContexts();
    $contexts[] = 'route.entity_uuid';

    foreach ($this->loadMessages() as $message) {
      $contexts = Cache::mergeContexts($contexts, $message->getCacheContexts());
    }

    return $contexts;
  }

  /**
   * Loads all notification messages.
   *
   * @return \Drupal\notification_message\Entity\NotificationMessageInterface[]
   *   All notification messages.
   */
  protected function loadMessages(): array {
    try {
      $context = $this->loadContexts();
      $storage = $this->entityTypeManager->getStorage('notification_message');

      $date = $this->now();
      $query = $storage->getQuery();
      $query->accessCheck(FALSE);
      $query->condition('publish_end_date', $date, '>=');
      $query->condition('publish_start_date', $date, '<=');

      if ($message_type = $this->getNotificationMessageType()) {
        $query->condition('type', (array) $message_type, 'IN');
      }
      $messages = $storage->loadMultiple($query->execute());

      return array_filter(
        $messages,
        static function (NotificationMessage $message) use ($context) {
          return $message->isPublished()
            && $message->access('view')
            && $message->evaluateConditions($context);
      });
    }
    catch (\Exception $exception) {
      watchdog_exception('notification_message', $exception);
    }

    return [];
  }

  /**
   * Load the runtime contexts.
   *
   * @return array
   *   An array of the runtime contexts.
   */
  protected function loadContexts(): array {
    $contexts = [];
    $repository = $this->contextRepository;
    $context_ids = array_keys($repository->getAvailableContexts());

    foreach ($repository->getRuntimeContexts($context_ids) as $name => $context) {
      $unqualified_context_id = substr($name, strpos($name, ':') + 1);
      $contexts[$unqualified_context_id] = $context;
    }

    return $contexts;
  }

  /**
   * Renders an array of notification_message entities.
   *
   * @param \Drupal\notification_message\Entity\NotificationMessageInterface[] $messages
   *   The messages to be rendered.
   *
   * @return array
   *   A render array representing notification messages.
   */
  protected function viewMessages(array $messages): array {
    $build = [];

    /** @var \Drupal\Core\Entity\EntityViewBuilder $view_builder */
    $view_builder = $this->entityTypeManager->getViewBuilder('notification_message');
    $display_mode = $this->getNotificationMessageDisplayMode();

    foreach ($messages as $message) {
      $build[] = $view_builder->view($message, $display_mode);
    }

    return $build;
  }

  /**
   * Get notification message display mode options.
   *
   * @return array
   *   An array of display mode options.
   */
  protected function getDisplayModeOptions() {
    return $this->entityDisplayRepository->getViewModeOptions('notification_message');
  }

  /**
   * Get the notification message type options.
   *
   * @return array
   *   An array of notification message types.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getNotificationMessageTypeOptions() {
    $options = [];

    foreach ($this->notificationMessageTypeStorage()->loadMultiple() as $id => $entity) {
      assert($entity instanceof NotificationMessageType);
      $options[$id] = $entity->label();
    }

    return $options;
  }

  /**
   * Determine if the notification dismiss is enabled for the selected types.
   *
   * @return bool
   *   Return TRUE if notification dismiss is enabled; otherwise FALSE.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function hasNotificationTypeDismissEnabled(): bool {
    $types = $this->getNotificationMessageType();

    if (!empty($types)) {
      /** @var \Drupal\notification_message\Entity\NotificationMessageType[] $entities */
      $entities = $this->notificationMessageTypeStorage()->loadMultiple($types);

      foreach ($entities as $entity) {
        if ($entity->getNotificationDismissShow()) {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

  /**
   * The current Drupal date & time.
   *
   * @return string
   *   The current datetime formatted string.
   */
  protected function now(): string {
    // Need to account for the date/time storage that is always UTC.
    return (new DrupalDateTime('now', DateTimeItemInterface::STORAGE_TIMEZONE))->format(
      DateTimeItemInterface::DATETIME_STORAGE_FORMAT
    );
  }

  /**
   * Notification message type storage instance.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   The notification message type storage.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function notificationMessageTypeStorage(): EntityStorageInterface {
    return $this->entityTypeManager->getStorage('notification_message_type');
  }

}
