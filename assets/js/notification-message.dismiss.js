(function ($, Drupal, Cookie, once) {
  'use strict';

  /**
   * Read cookie data.
   *
   * @param name
   *   The cookie name.
   *
   * @returns {any}
   */
  function readCookieData(name) {
    let cookieData = Cookie.get(name);
    if (typeof cookieData === "string") {
      return JSON.parse(cookieData);
    }
    return cookieData;
  }

  /**
   * Merge cookie data.
   *
   * @param name
   *   The cookie name.
   * @param data
   *   The cookie data.
   */
  function mergeCookieData(name, data) {
    let cookieData = readCookieData(name) || [];
    let mergedData = $.merge([data], cookieData);

    Cookie.set(
      name,
      JSON.stringify(mergedData.filter(function (element, index, self) {
        return index === self.indexOf(element);
      }))
    );
  }

  Drupal.behaviors.notificationMessage = {
    attach: function (context) {

      once('notificationMessage', 'html', context).forEach(function () {
        let cookieData = readCookieData('notificationMessagesClosed') || [];
        $('.notification-messages .message').each(function () {
          let messageId = $(this).data('message-id');
          if ($.inArray(messageId, cookieData) === -1) {
            if ($(this).parent().is(':hidden')) {
              $(this).parent().show();
            }
            $(this).show();
          } else {
            $(this).remove();
          }
        });
      })

      $('.notification-messages .message a.message__close', context).click(function (event) {
        event.preventDefault();
        let $parent = $(this).parent();
        let messageId = $parent.data('message-id');
        let notificationCount = $('.notification-messages > div').length;

        $('#notification-message-aria-announcement').text(
          'Notification message closed'
        );

        if(notificationCount > 1){
          $parent.remove();
        } else {
          $('.notification-messages').remove();
        }

        if (messageId !== undefined) {
          mergeCookieData(
            'notificationMessagesClosed',
            messageId
          );
        }
      });

    }
  }
}(jQuery, Drupal, window.Cookies, once));
